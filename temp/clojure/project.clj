(defproject clj-ortools "0.1.0-SNAPSHOT"
  :description "Operation Research Tools Template"
  :dependencies [[org.clojure/clojure "1.8.0"]]
  :jvm-opts ["-Djava.library.path=resources/lib"]
  :resource-paths ["resources" "resources/lib/com.google.ortools.jar" "resources/lib/protobuf.jar"]
  :native-path "resources/lib"
  :aot [clj-ortools.core] ;; <-- Place all solver namespaces in here
  :repl-options {:init-ns clj-ortools.core}
  :main clj-ortools.core)
