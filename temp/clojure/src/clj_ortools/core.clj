(ns clj-ortools.core
  (:gen-class)
  (:import (com.google.protobuf GeneratedMessageV3)
           (com.google.ortools.sat CpModel
                                   CpSolver
                                   CpSolverStatus
                                   IntVar)))

(defn load-jni []
  (System/loadLibrary "jniortools"))

(defn -main []
  (load-jni)
  (let [model (CpModel.)
        num-vals 3
        x (.newIntVar model 0 (- num-vals 1) "x")
        y (.newIntVar model 0 (- num-vals 1) "y")
        z (.newIntVar model 0 (- num-vals 1) "z")]
    (.addAllDifferent model (into-array IntVar [x y z]))
    (let [solv (CpSolver.)
          status (.solve solv model)]
      (cond
          (= status CpSolverStatus/FEASIBLE) (do (println "x = " (.value solv x))
                                                 (println "y = " (.value solv y))
                                                 (println "z = " (.value solv z)))
          :default (println "No Solutions.")))))
