# Database Research: Optimization Research Tools (OR-Tools)

## Google's Optimization Research Tools (OR-Tools)
The webpages for Google's OR-Tools resources can be accessed [here](https://developers.google.com/optimization/),
part of the Google Developers site.

Google provides documentation on OR-Tools in the form of guides and C++ API References. These
sources can be found at the following locations:

- [OR-Tools Guides](https://developers.google.com/optimization/introduction/overview)
- [OR-Tools C++ API References](https://developers.google.com/optimization/reference/)
- [google.github.io OR-Tools reference manuals](https://google.github.io/or-tools/)

### Download OR-Tools
For Python 2.7 or 3.5+, OR-Tools can be downloaded either via the following `pip` command or by downloading 
the [wheel file](https://pypi.org/project/ortools/#files) then installing it using `pip` from that file.

```
pip install ortools
```

For C++, Java, or C#, OR-Tools can be downloaded either as a binary distribution from one of the links 
listed [here](https://developers.google.com/optimization/install/#binary) or in source form from 
[here](https://developers.google.com/optimization/install/#source).

### Required Library and Header Files
For building/running the source (excluding Python) it is required to obtain the library and/or header 
files. For this, it is recommended to download a binary distribution for the appropriate OS. Library files
in these distributions are located in `lib` and header files are located in `include`.

For C++ template, copy both the `lib` and the `include` directories into the root working directory. 
C++ requires both to properly build then like with the other languages only needs the libraries to
run the compiled binary.

For the Clojure template, copy the `lib` directory into `resources/lib`. Ensure that all Clojure namespaces 
which contain imports of OR-Tools libraries are added to `:aot` (ahead-of-time compilation) collection; 
otherwise, these libraries will not import correctly.

Google Inc. reserves all rights for Optimization Research Tools
