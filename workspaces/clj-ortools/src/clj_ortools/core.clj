(ns clj-ortools.core
  (:gen-class)
  (:import (com.google.protobuf GeneratedMessageV3)
           (com.google.ortools.sat CpModel
                                   CpSolver
                                   CpSolverStatus
                                   IntVar)))

(defn load-jni []
  (System/loadLibrary "jniortools"))
