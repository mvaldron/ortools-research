(ns nqueens.core)

;; (defn load-jni []
;;   (System/loadLibrary "jniortools"))

;; (defn -main []
;;   (load-jni)
;;   (let [model (CpModel.)
;;         num-vals 3
;;         v (cp/create-cpobj (java.util.Date.))
;;         t (.newIntVarFromDomain model (Domain/fromValues (into-array Long/TYPE #{1 2 4 9})) "t")
;;         x (.newIntVar model 0 (- num-vals 1) "x")
;;         y (.newIntVar model 0 (- num-vals 1) "y")
;;         z (.newIntVar model 0 (- num-vals 1) "z")]
;;     (.addAllDifferent model (into-array IntVar [x y z t]))
;;     (let [solv (CpSolver.)
;;           status (.solve solv model)]
;;       ;; (println (schema/validate var-types :int))
;;       (cond
;;           (= status CpSolverStatus/FEASIBLE) (do (println "x = " (.value solv x))
;;                                                  (println "y = " (.value solv y))
;;                                                  (println "z = " (.value solv z))
;;                                                  (println "t = " (.value solv t)))
;;           :default (println "No Solutions.")))))
