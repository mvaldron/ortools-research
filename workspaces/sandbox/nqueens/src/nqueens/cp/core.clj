(ns nqueens.cp.core
  (:gen-class)
  (:require [clojure.pprint :refer [pprint]]
            [clojure.string :as st]
            [clojure.set :refer [map-invert]])
  (:import (com.google.protobuf GeneratedMessageV3)
           (com.google.ortools.sat CpModel
                                   CpSolver
                                   CpSolverStatus
                                   IntVar
                                   LinearExpr
                                   Literal)
           (com.google.ortools.util Domain)))

(defonce ^:private jni-file
  "resources/lib/libjniortools.so")

(defonce ^:private jni-loaded-err-msg
  "already loaded in another classloader")

(defonce ^:private status-map {:feasible CpSolverStatus/FEASIBLE
                               :optimal CpSolverStatus/OPTIMAL
                               :infeasible CpSolverStatus/INFEASIBLE
                               :model-invalid CpSolverStatus/MODEL_INVALID
                               :unrecognized CpSolverStatus/UNRECOGNIZED
                               :unknown CpSolverStatus/UNKNOWN})

(defonce ^:private assoc-map {:or `-or
                              :diff `diff
                              :> `(-comparator -greater)
                              :< `(-comparator -less)
                              :>= `(-comparator -greater-or-equal)
                              :<= `(-comparator -less-or-equal)
                              := `(-comparator -equal)
                              :not= `(-comparator -not-equal)
                              :+ `-add
                              :- `-sub})

(declare var->)

(def model (atom nil))

(defn mk-cp-model []
  (try
    (System/load (-> jni-file
                     java.io.File.
                     .getAbsolutePath))
    (catch java.lang.UnsatisfiedLinkError e#
      (when (not (st/includes? (.getMessage e#)
                               jni-loaded-err-msg))
        (do (println (.getMessage e#))
            (System/exit 1)))))
  (reset! model {:model (CpModel.)
                 :solver (CpSolver.)}))

;; Getters
(declare ->name)
(declare ->domain)
(declare ->index)
(declare ->description)
(declare ->value)
(declare ->variable)
(declare ->variable-list)

(defn ->name
  [obj]
  (-> obj :data .getName))

(defn ->domain
  [obj]
  (let [get-proto #(-> % :data .getBuilder)];; (if (map? (:data %))
    ;;  (-> % :data .getBuilder)
    ;;  (:data %))]
    (-> obj
        get-proto
        .getDomainList
        vec)))

(defn ->index
  [obj]
  (-> obj :data .getIndex))

(defn ->description
  [obj]
  (-> obj :data .getShortString))

(defn ->value
  [obj]
  (-> @model :solver (.value (:data obj))))

;; TODO: create way of saving/fetching metadata from model
(defn ->variable
  [idx]
  (-> (->variable-list) (nth idx)))

(defn ->variable-list
  ([] (->variable-list (:model @model)))
  ([obj]
   (let [get-from-model #(->> %
                              .getVariablesList
                              (mapv (partial assoc {} :data)))
         get-from-const #(->> %
                              .getLinearBuilder
                              .getVarsList
                              (mapv ->variable))
         obj# (if (map? obj) (:data obj) obj)
         b (.getBuilder obj#)]
     (if (instance? CpModel obj#)
       (get-from-model b)
       (get-from-const b)))))

;; CP Operations
(defn ^:private add-metadata
  [var# meta#]
  (if (or (nil? meta#)
          (empty? meta#))
    var#
    (assoc var# :metadata (apply hash-map meta#))))

(defn ^:private mk-cp-var
  [domain name# meta#]
  (let [new-var# #(.newIntVar %1 %2 %3 %4)
        expr# (concat [(:model @model)] domain [name#])
        v# {:data (apply new-var# expr#)}]
    (add-metadata v# meta#)))

(defn ^:private mk-cp-val
  [val# name# meta#]
  (let [new-var# #(.newIntVar %1 %2 %2 %3)
        new-val# #(.newConstant %1 %2)
        expr# (-> [(:model @model) val#]
                  (#(if (nil? name#)
                      %
                      (conj % name#))))
        v# {:data (apply (if (nil? name#)
                           new-val#
                           new-var#) expr#)}]
    (add-metadata v# meta#)))

(defn -or
  [& args]
  (let [cs (map :data args)
        cvars (for [i (-> cs count range)]
                (.newIntVar (:model @model)
                            0 1 (name `cvar#)))]
    (doseq [i (-> cs count range)
            :let [c (nth cs i)
                  v (nth cvars i)]]
      (.onlyEnforceIf c v))
    (let [cor (.addBoolOr (:model @model)
                          (into-array IntVar cvars))]
      {:data cor})))

(defn -comparator
  [f lh rh]
  (let [fetch-var-or-val# #(if (int? %) % (:data %))
        lh# (fetch-var-or-val# lh)
        rh# (fetch-var-or-val# rh)]
    (assert (not (and (int? lh#) (int? rh#))))
    (if (int? lh#)
      (f (.newConstant (:model @model) lh#) rh#)
      (f lh# rh#))))

(defn -equal
  [lh rh]
  {:data (.addEquality (:model @model) lh rh)})

(defn -not-equal
  [lh rh]
  {:data (.addDifferent (:model @model) lh rh)})

(defn -less-or-equal
  [lh rh]
  {:data (.addLessOrEqual (:model @model) lh rh)})

(defn -greater-or-equal
  [lh rh]
  {:data (.addGreaterOrEqual (:model @model) lh rh)})

(defn -less
  [lh rh]
  {:data (.addLessOrEqualWithOffset (:model @model) lh rh 1)})

(defn -greater
  [lh rh]
  {:data (.addGreaterOrEqualWithOffset (:model @model) lh rh 1)})

(defn diff
  [vs]
  {:data (.addAllDifferent (:model @model)
                           (into-array IntVar
                                       (map :data vs)))})

(defn -sum
  ([vs]
   (let [vars (into-array IntVar (map :data vs))]
     (LinearExpr/sum vars)))
  ([op vs]
   (let [vars (into-array IntVar (map :data vs))
         coefs (into-array Long/TYPE
                           (concat [1]
                                   (repeat (-> vars
                                               rest
                                               count)
                                           (op 1))))]
     (LinearExpr/scalProd vars coefs))))

(defn -add
  [& args]
  (let [sum-domains (fn [domains]
                      (let [sum #(->> (map %1 %2)
                                      (apply +))
                            new-min (sum first domains)
                            new-max (sum second domains)]
                        [new-min new-max]))
        is-name? (-> args first string?)
        name# (if is-name? (first args) (name `svar#)) 
        vars (if is-name? (rest args) args)
        domain (sum-domains (map ->domain vars))
        result-var (var-> domain name#)
        assign-cs (-equal (:data result-var) (-sum vars))]
    result-var))

(defn -sub
  [& args]
  (let [sub-domains (fn [domains]
                      (let [sub #(->> (map %1 %2)
                                      (apply -))
                            new-min (sub first domains)
                            new-max (sub second domains)]
                        [new-min new-max]))
        is-name? (-> args first string?)
        name# (if is-name? (first args) (name `svar#)) 
        vars (if is-name? (rest args) args)
        domain (sub-domains (map ->domain vars))
        result-var (var-> domain name#)
        assign-cs (-equal (:data result-var) (-sum - vars))]
    result-var))
  
(defn ^:private build-inner-cs
  [item]
  (if (list? item)
    (let [[sym# & expr#] item
          k# (keyword sym#)
          begin-expr# (k# assoc-map)
          call-expr# (if (seq? begin-expr#)
                       begin-expr#
                       (list begin-expr#))]
      `(~@call-expr# ~@(map build-inner-cs expr#)))
    (cond (int? item) `(val-> ~item) 
          :default item)))

(defn var->
  [domain name# & meta#]
  (mk-cp-var domain name# meta#))

(defn val->
  [val# & [name# & meta#]]
  (mk-cp-val val# name# meta#))

(defmacro c->
  [[sym & expr]]
  (let [k# (keyword sym)
        begin-expr# (k# assoc-map)
        call-expr# (if (seq? begin-expr#) begin-expr# (list begin-expr#))]
    `(~@call-expr# ~@(map build-inner-cs expr))))

;; Solver Procedures
(defn solve
  [dresult & vars]
  (let [dresult-obj (dresult status-map)
        result (.solve (:solver @model)
                       (:model @model))]
    (if (= result dresult-obj)
      (do
        (println (format "Solution is '%s'." dresult))
        (when (-> vars nil? not)
          (->> vars
               (mapv #(hash-map (-> % ->name keyword)
                                (->value %)))
               (apply merge))))
      (-> (str "Got no solution "
               "with '%s' status. "
               "Got '%s' status instead.")
          (format dresult (-> status-map
                              map-invert
                              (get result)))
          println))))
