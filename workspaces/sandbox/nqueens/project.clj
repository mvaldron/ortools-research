(defproject nqueens "0.1.0-SNAPSHOT"
  :description "Operation Research Tools Template"
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [prismatic/schema "1.1.12"]
                 [clojupyter "0.2.3"]
                 [seancorfield/depstar "0.3.3"]
                 [com.attendify/schema-refined "0.3.0-alpha4"]]
  :jvm-opts ["-Djava.library.path=resources/lib"]
  :resource-paths ["resources" "resources/lib/com.google.ortools.jar" "resources/lib/protobuf.jar"]
  :native-path "resources/lib"
  :aot [nqueens.cp.core]
  :repl-options {:init-ns nqueens.core})
