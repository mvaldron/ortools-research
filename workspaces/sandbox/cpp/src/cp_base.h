#ifndef _cp_base_H
#define _cp_base_H

using namespace std;
using namespace operations_research;
using namespace operations_research::sat;

namespace cp_base {
  template <class T>
  class CpDomain {
  private:
    set<T> m_members;
    Domain m_d;
  public:
    CpDomain ();
    CpDomain (T, T);
    CpDomain (T*);
    Domain GetDomain() const;
    vector<T> GetDomainMembers() const;
    CpDomain<T> U(const CpDomain<T>);
    CpDomain<T> operator-(const CpDomain<T>);
  };

  template <>
  class CpDomain <int> {
  private:
    set<int> m_members;
    Domain m_d;
  public:
    CpDomain ();
    CpDomain (int, int);
    CpDomain (int*);
    Domain GetDomain() const;
    vector<int> GetDomainMembers() const;
    CpDomain<int> U(const CpDomain<int>);
    CpDomain<int> operator-(const CpDomain<int>);
  };

  template <>
  class CpDomain <char> {
  private:
    set<char> m_members;
    Domain m_d;
  public:
    CpDomain ();
    CpDomain (char*);
    Domain GetDomain() const;
    vector<char> GetDomainMembers() const;
    CpDomain<char> U(const CpDomain<char>);
    CpDomain<char> operator-(const CpDomain<char>);
  };
  
  template <class T>
  class CpVar {
  private:
    string m_name;
    IntVar m_var;
    CpDomain<T> m_domain;
    CpModelBuilder* m_model;
  public:
    CpVar (CpModelBuilder*, string);
    string GetName () const;
    T GetValue (const CpSolverResponse);
    // Temp Function(s) for Dev
    IntVar GetVar ();
    CpVar<T> operator<<(const CpDomain<T>);
  };

  template <>
  class CpVar <int> {
  private:
    string m_name;
    IntVar m_var;
    CpDomain<int> m_domain;
    CpModelBuilder* m_model;
  public:
    CpVar (CpModelBuilder*, string); 
    string GetName () const;
    int GetValue (const CpSolverResponse);
    // Temp Function(s) for Dev
    IntVar GetVar ();
    CpVar<int> operator<<(const CpDomain<int>);
    Constraint operator==(const CpVar<int>);
    Constraint operator==(const long int);
    Constraint operator!=(const CpVar<int>);
    Constraint operator!=(const long int);
    Constraint operator>(const CpVar<int>);
    Constraint operator>(const long int);
    Constraint operator<(const CpVar<int>);
    Constraint operator<(const long int);
    Constraint operator>=(const CpVar<int>);
    Constraint operator>=(const long int);
    Constraint operator<=(const CpVar<int>);
    Constraint operator<=(const long int);
    CpVar<int> operator+(const CpVar<int>);
  };

  template <>
  class CpVar <bool> {
  private:
    string m_name;
    IntVar m_var;
    CpDomain<bool> m_domain;
    CpModelBuilder* m_model;
  public:
    CpVar (CpModelBuilder*, string); 
    string GetName () const;
    int GetValue (const CpSolverResponse);
    // Temp Function(s) for Dev
    IntVar GetVar ();
    Constraint IsTrue();
    Constraint IsFalse();
    Constraint operator==(const CpVar<bool>);
    Constraint operator==(const bool);
    Constraint operator!=(const CpVar<bool>);
    Constraint operator!=(const bool);
    CpVar<int> operator+(const CpVar<bool>);
  };

  template <>
  class CpVar <string> {
  private:
    string m_name;
    int m_slength;
    vector<IntVar> m_var;
    CpModelBuilder* m_model;
  public:
    CpVar (CpModelBuilder*, string);
    string GetName () const;
    string GetValue (const CpSolverResponse);
    // Temp Function(s) for Dev
    IntVar* GetVar ();
    CpVar<string> operator<<(const CpDomain<char>);
  };

  template <class T>
  class CpConstraint <T> {
  private:
    Constraint m_constraint;
  public:
    CpConstraint ();
    CpConstraint (Constraint*);
    string GetExpr ();
    CpConstraint OnlyEnforceIf(absl::Span<const BoolVar>);
    CpConstraint OnlyEnforceIf(BoolVar);
  };
}

#endif
