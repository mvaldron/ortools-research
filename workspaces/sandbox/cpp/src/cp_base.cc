#include <climits>
#include <vector>
#include <set>
#include <algorithm>
#include <cassert>
#include <ortools/sat/cp_model.h>
#include "gen.h"
#include "cp_base.h"

using namespace std;
using namespace operations_research;
using namespace operations_research::sat;

// FOR DEBUG
// ---------------------------------------------------
void PrintThings(IntVar* vs, int c) {
  for (int i = 0; i < c; i++)
    LOG(INFO) << vs[i];
}

void PrintThing(string v) {
  LOG(INFO) << v;
}
// ---------------------------------------------------

namespace cp_base {
  // CpDomain
  // <int>
  CpDomain<int>::CpDomain() {
    const Domain d(INT_MIN, INT_MAX);
    m_members.insert(m_members.end(), INT_MIN);
    m_members.insert(m_members.end(), INT_MAX);
    m_d = d;
  }

  CpDomain<int>::CpDomain(int l_bound, int u_bound) {
    const Domain d((long int)l_bound, (long int)u_bound);
    m_members.insert(m_members.end(), l_bound);
    m_members.insert(m_members.end(), u_bound);
    m_d = d;
  }

  CpDomain<int>::CpDomain(int* d_vals) {
    vector<long int> or_domain_vals;
    for (int i = 0; i < (sizeof(d_vals)/sizeof(*d_vals)); i++) {
      m_members.insert(m_members.end(), d_vals[i]);
      or_domain_vals.push_back((long int)d_vals[i]);
    }
    m_d = Domain::FromValues(or_domain_vals);
  }

  Domain CpDomain<int>::GetDomain() const {
    return m_d;
  }

  vector<int> CpDomain<int>::GetDomainMembers() const {
    vector<int> vals(m_members.begin(), m_members.end());
    return vals;
  }

  // <char>
  CpDomain<char>::CpDomain() {
    vector<long int> or_domain_vals;
    m_members = fetch_char_set();
    for (char c : m_members)
      or_domain_vals.push_back((long int)c);
    m_d = Domain::FromValues(or_domain_vals);
  }

  CpDomain<char>::CpDomain(char* d_vals) {
    vector<long int> or_domain_vals;
    for (int i = 0; i < (sizeof(d_vals)/sizeof(*d_vals)); i++) {
      m_members.insert(m_members.end(), d_vals[i]);
      or_domain_vals.push_back((long int)d_vals[i]);
    }
    m_d = Domain::FromValues(or_domain_vals);
  }
  
  Domain CpDomain<char>::GetDomain() const {
    return m_d;
  }

  vector<char> CpDomain<char>::GetDomainMembers() const {
    vector<char> vals(m_members.begin(), m_members.end());
    return vals;
  }

  // CpVar
  // <int>
  CpVar<int>::CpVar(CpModelBuilder* m, string name) {
    m_name = name;
    m_model = m;
  }

  string CpVar<int>::GetName() const {
    return m_name;
  }

  int CpVar<int>::GetValue(const CpSolverResponse r) {
    return SolutionIntegerValue(r,m_var);
  }

  // CpVar<int> (set var) = CpVar<int> << CpDomain<int>
  CpVar<int> CpVar<int>::operator<<(const CpDomain<int> d) {
    m_domain = d;
    m_var = m_model->NewIntVar(d.GetDomain()).WithName(m_name);
    return *this;
  }
  
  Constraint CpVar<int>::operator==(const CpVar<int> other) {
    return m_model->AddEquality(m_var, other.m_var);
  }

  Constraint CpVar<int>::operator==(const long int val) {
    return m_model->AddEquality(m_var, val);
  }

  Constraint CpVar<int>::operator!=(const CpVar<int> other) {
    return m_model->AddNotEqual(m_var, other.m_var);
  }

  Constraint CpVar<int>::operator!=(const long int val) {
    return m_model->AddNotEqual(m_var, val);
  }

  Constraint CpVar<int>::operator>(const CpVar<int> other) {
    return m_model->AddGreaterThan(m_var, other.m_var);
  }

  Constraint CpVar<int>::operator>(const long int val) {
    return m_model->AddGreaterThan(m_var, val);
  }

  Constraint CpVar<int>::operator<(const CpVar<int> other) {
    return m_model->AddLessThan(m_var, other.m_var);
  }

  Constraint CpVar<int>::operator<(const long int val) {
    return m_model->AddLessThan(m_var, val);
  }

  Constraint CpVar<int>::operator>=(const CpVar<int> other) {
    return m_model->AddGreaterOrEqual(m_var, other.m_var);
  }

  Constraint CpVar<int>::operator>=(const long int val) {
    return m_model->AddGreaterOrEqual(m_var, val);
  }

  Constraint CpVar<int>::operator<=(const CpVar<int> other) {
    return m_model->AddLessOrEqual(m_var, other.m_var);
  }

  Constraint CpVar<int>::operator<=(const long int val) {
    return m_model->AddLessOrEqual(m_var, val);
  }

  CpVar<int> CpVar<int>::operator+(const CpVar<int> other) {
    CpDomain<int>* D;
    vector<int> this_d = m_domain.GetDomainMembers();
    vector<int> other_d = other.m_domain.GetDomainMembers();
    vector<int> d_vals;
    assert(this_d.size() == other_d.size());
    for (int i = 0; i < this_d.size(); i++)
      d_vals.push_back(this_d.at(i) + other_d.at(i));
    if (d_vals.size() == 2)
      D = new CpDomain<int>(d_vals.front(), d_vals.back());
    else
      D = new CpDomain<int>(d_vals.data());
    CpVar<int> result_var(m_model, GetName() + " + " + other.GetName());
    result_var = result_var << *D;
    m_model->AddEquality(LinearExpr::ScalProd({result_var.m_var}, {1}),
                         LinearExpr::ScalProd({m_var, other.m_var},
                                              {1, 1}));
    return result_var;
  }

  // Temp Function(s) for Dev
  IntVar CpVar<int>::GetVar() {
    return m_var;
  }

  // <string>
  CpVar<string>::CpVar(CpModelBuilder* m, string name) {
    m_name = name;
    m_model = m;
  }
  
  // TODO
  // CpVar<string>::CpVar(CpModelBuilder* m, string val, string name) {
  //   Domain* d;
  //   m_name = name;
  //   m_slength = val.length();
  //   for (int i = 0; i < val.length(); i++) {
  //     d = new Domain((long int)val[i]);
  //     m_var.push_back(m->NewIntVar(*d).WithName(m_name + "_" + to_string(i)));
  //   }
  //   m_model = m;
  // }
  
  string CpVar<string>::GetName() const {
    return m_name;
  }
  
  // TODO
  // CpVar<string> CpVar<string>::operator<<(const CpDomain<string>& d) {
  //   m_domain = d;
  //   m_var = m_model->NewIntVar(d).WithName(m_name);
  //   return this;
  // }
  
  string CpVar<string>::GetValue(const CpSolverResponse r) {
    string value;
    for (int i = 0; i < m_var.size(); i++) {
      value += (char)SolutionIntegerValue(r,m_var.at(i));
    }
    return value;
  }

  // Temp Function(s) for Dev

  IntVar* CpVar<string>::GetVar() {
    return m_var.data();
  }
  //-------------------------
}
