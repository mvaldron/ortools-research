#include <climits>
#include <set>
#include <algorithm>
#include <cctype>
#include <iterator>
#include "gen.h"

using namespace std;

struct c_uni {
  int curr;
  c_uni() { curr = 0; }
  int operator()() { return ++curr; }
} RangeGen;

set<char> fetch_char_set() {
  set<char> crange, cset;
  auto f = [](char c) { return isspace(c) || isprint(c); };
  generate_n(inserter(crange, crange.begin()), CHAR_MAX+1, RangeGen);
  copy_if(crange.begin(), crange.end(), inserter(cset, cset.begin()), f);
  return cset;
}

set<char> fetch_subset(const set<char> superset, int (*f)(char)) {
  set<char> subset;
  copy_if(superset.begin(), superset.end(),
          inserter(subset, subset.begin()), f);
  return subset;
}

set<char> fetch_alpha_subset(const set<char> superset) {
  return fetch_subset(superset, [](char c) { return isalpha(c); });
}

set<char> fetch_whitespace_subset(const set<char> superset) {
  return fetch_subset(superset, [](char c) { return isspace(c); });
}

set<char> fetch_punct_subset(const set<char> superset) {
  return fetch_subset(superset, [](char c) { return ispunct(c); });
}
