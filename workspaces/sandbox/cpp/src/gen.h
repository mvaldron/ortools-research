#ifndef _gen_H
#define _gen_H

using namespace std;

set<char> fetch_char_set();
set<char> fetch_subset(const set<char>, int (*)(char));
set<char> fetch_alpha_subset(const set<char>);
set<char> fetch_whitespace_subset(const set<char>);
set<char> fetch_punct_subset(const set<char>);

#endif
