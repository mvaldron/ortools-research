#include <set>
#include <algorithm>
#include <iostream>
#include <ortools/sat/cp_model.h>
#include "gen.h"
#include "cp_base.h"

using namespace std;
using namespace cp_base;

namespace operations_research
{
  namespace sat
  {
    int Run()
    {
      CpModelBuilder m;
      const CpDomain<int> D(1, 2);
      CpVar<int> x(&m, "x");
      CpVar<int> y(&m, "y");
      x = x << D;
      y = y << D;
      CpVar<int> z = x + y;
      Constraint c = x < y;
      const CpSolverResponse resp = Solve(m.Build());
      LOG(INFO) << CpSolverResponseStats(resp);
      if (resp.status() == CpSolverStatus::FEASIBLE)
      {
        LOG(INFO) << "x = " << x.GetValue(resp);
        LOG(INFO) << "y = " << y.GetValue(resp);
        LOG(INFO) << "z = " << z.GetValue(resp);
      }
      return EXIT_SUCCESS;
    }
  }
}

int main() { return operations_research::sat::Run(); }
