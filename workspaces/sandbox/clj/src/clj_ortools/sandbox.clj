(ns clj-ortools.sandbox
  (:gen-class)
  (:require [clojure.string :as string]
            [clojure.pprint :refer [pprint]]
            [clojure.test :refer [function?]]
            [cljcc :refer [make-lexer make-parser make-combined]])
  (:import (com.google.protobuf GeneratedMessageV3)
           (com.google.ortools.sat CpModel
                                   CpSolver
                                   CpSolverStatus
                                   IntVar)))

(def ph-marks ":")

(def tokens
    #{  { :name :plus    :pattern "+" }
        { :name :mult    :pattern "*" }
        { :name :lparen  :pattern "(" }
        { :name :rparen  :pattern ")" }
        { :name :int     :pattern #"\d+" }
        { :ignore true   :pattern #"\s+" }})

(defn load-jni []
  (System/loadLibrary "jniortools"))

(defn func-update [pattern args]
  (cond (> (count args) 0) (pattern args)
        :default (pattern)))

(defn str-update [pattern chs]
  (if (> (count chs) 0)
    (reduce (fn [s c]
              (let [p (string/replace (:pattern c) #"\\" "\\\\\\\\")]
                (string/replace
                 s
                 (re-pattern (str ph-marks (:name c) ph-marks))
                 p))) pattern chs)
    pattern))

(defn pattern-update [token]
  (let [pattern (:pattern token)]
    (assoc token :pattern (re-pattern pattern))))

(defn eval-token [m reserved tokens token prop]
  (let [pattern (eval (:pattern prop))]
    (assoc tokens token
           (assoc prop :pattern (cond
                                  (and (function? pattern)
                                       (= token (:sym-key m)))
                                  (func-update pattern reserved)
                                  :default
                                  (str-update
                                   pattern
                                   (filter #(-> % :char nil?) (vals tokens))))))))

(defn eval-model [m]
  (let [tokens (:token-types m)
        token-prop (vals tokens)
        token-keys (keys tokens)
        reserved (map :char (filter #(= (:type %) "keyword") token-prop))
        p-update-token (partial eval-token m reserved)]
    (-> (assoc m :token-types (reduce-kv p-update-token {} tokens))
        (#(assoc m :token-types (zipmap token-keys (map pattern-update (-> % :token-types vals))))))))


(defn get-comps [expr-char expr]
  (map read-string
       (filter some? (-> expr
                         (string/replace (re-pattern (str "\\" expr-char)) "")
                         string/trim
                         (string/split #"\s+")))))

(defn -main [& args]
  (let [m (-> (slurp "resources/model.edn")
              read-string
              eval-model)
        ex (slurp (first args))
        add (-> m :token-types :add)
        matches (-> add :pattern (re-matches ex))
        match-add? (some? matches)]
    (if match-add?
      (do (pprint ex)
          (let [lex (make-lexer tokens)]
            (println (lex "(12 + 34) * 56 ")))
          (-> add :action eval (#(% (get-comps (:char add) ex))) pprint))
      (println "Invalid expression. Check language scheme."))))
