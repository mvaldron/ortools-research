(ns clj-ortools.examples.scheduling.employee.core
  (:gen-class)
  (:require [clj-ortools.core :refer [load-jni]]
            [clj-ortools.vars.ds :refer [cpvar->ds]])
  (:import (com.google.protobuf GeneratedMessageV3)
           (com.google.ortools.sat CpModel
                                   CpSolver
                                   CpSolverStatus
                                   IntVar)))

(defn mk-cp []
  {:model (CpModel.)
   :solver (CpSolver.)})

(defn mk-int-var [cp r n]
  (cond
    (= (type r) java.lang.Long)
    (.newIntVar (:model cp) r r n)
    (= (type r) (type {}))
    (let [[l u] (vals r)]
      (.newIntVar (:model cp) l u n))
    :default
    (let [[l u] (vec r)]
      (.newIntVar (:model cp) l u n))))

(defn var-arr [coll]
  (into-array IntVar coll))

(defn mk-var-coll
  ([cp d n] (mk-var-coll cp d n "x_"))
  ([cp d n prefix] (mk-var-coll cp d 0 n prefix))
  ([cp d l n prefix]
   (vec
    (for [i (range l n)]
      (mk-int-var cp
                  d
                  (format (str prefix "%d") i))))))

(defn mk-var-arr
  ([cp d n] (mk-var-arr cp d n "x_"))
  ([cp d n prefix] (mk-var-arr cp d 0 n prefix))
  ([cp d l n prefix]
   (var-arr (mk-var-coll [cp d l n prefix]))))

(defn cp-solve [cp]
  (.solve (:solver cp) (:model cp)))

(defn cp-solve-all [cp sol-callback]
  (.solveWithSolutionCallback (:model cp) sol-callback))

(defn result [cp var]
  (.value (:solver cp) var))

(defn add-cs [cp xs]
  (.addAllDifferent (:model cp) xs))

(defn mk-shifts-tbl [cp nums]
  (let [num-of (zipmap [:employees :days :shifts] nums)]
    (-> (mk-var-coll cp [0 1] (* (:employees num-of)
                                 (:days num-of)
                                 (:shifts num-of)))
        (#(partition (:shifts num-of) %))
        (#(partition (:days num-of) %)))))

(defn -main [num-n num-s num-d]
  (load-jni)
  (let [n-range (range num-n)
        s-range (range num-s)
        d-range (range num-d)
        cp-model (mk-cp)
        xs (mk-shifts-tbl cp-model [num-n num-d num-s])]
    (println xs)))
    ;;     cs (add-cs cp-model xs)
    ;;     status (cp-solve cp-model)]
    ;; (cond
    ;;   (= status CpSolverStatus/FEASIBLE)
    ;;   (doseq [x xs
    ;;           :let [xname (.getName x)]]
    ;;     (println (format (str xname " = %d") (result cp-model x))))
    ;;   :default
    ;;   (println "No Solutions Found."))))
