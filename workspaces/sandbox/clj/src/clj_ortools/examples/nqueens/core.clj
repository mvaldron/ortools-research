(ns clj-ortools.examples.nqueens.core
  (:gen-class)
  (:require [clj-ortools.core :refer [load-jni]]
            [clj-ortools.vars.ds :refer [cpvar->ds]])
  (:import (com.google.protobuf GeneratedMessageV3)
           (com.google.ortools.sat CpModel
                                   CpSolver
                                   CpSolverStatus
                                   IntVar
                                   CpSolverSolutionCallback)))

(defn mk-cp []
  {:model (CpModel.)
   :solver (CpSolver.)})

(defn mk-int-var [cp r n]
  (cond
    (= (type r) java.lang.Long)
    (.newIntVar (:model cp) r r n)
    (= (type r) (type {}))
    (let [[l u] (vals r)]
      (.newIntVar (:model cp) l u n))
    :default
    (let [[l u] (vec r)]
      (.newIntVar (:model cp) l u n))))

(defn var-arr [coll]
  (into-array IntVar coll))

(defn mk-var-arr
  ([cp d n] (mk-var-arr cp d n "x_"))
  ([cp d n prefix] (mk-var-arr cp d 0 n prefix))
  ([cp d l n prefix]
   (var-arr (for [i (range l n)]
              (mk-int-var cp
                          d
                          (format (str prefix "%d") i))))))

(defn cp-solve [cp]
  (.solve (:solver cp) (:model cp)))

(defn cp-solve-all [cp sol-callback]
  (.searchAllSolutions (:solver cp) (:model cp) (:proxy sol-callback)))

(defn result [cp var]
  (.value (:solver cp) var))

(defn get-dag [cp xs bsize rid]
  (for [j (range bsize)
        :let [q1 (mk-int-var cp [0 (* 2 bsize)] (format "diag1_%d" rid))
              q2 (mk-int-var cp [(- bsize) bsize] (format "diag2_%d" rid))]]
    (do
      (.addEqualityWithOffset (:model cp) (nth xs j) q1 j)
      (.addEqualityWithOffset (:model cp) (nth xs j) q2 (- j))
      [q1 q2])))

(defn add-cs [cp xs bsize]
  (.addAllDifferent (:model cp) xs)
  (doseq [i (range bsize)]
    (let [d (get-dag cp xs bsize i)
          d1 (-> (map #(nth % 0) d)
                 (var-arr))
          d2 (-> (map #(nth % 1) d)
                 (var-arr))]
      (.addAllDifferent (:model cp) d1)
      (.addAllDifferent (:model cp) d2))))

(defn solution-printer [vs]
  (for [v vs
        :let [bsize (count vs)
              row-vec (vec (repeat bsize "_"))]]
    (-> (assoc row-vec (int v) "Q")
        (#(clojure.string/join " " %)))))

(defn print-solution
  ([vs soln]
   (println (str "Solution #" soln ":"))
   (print-solution vs))
  ([vs]
   (do
     (-> (solution-printer vs)
         (#(map (fn [s] (str s "\n")) %))
         (#(apply println %))) 
     (println))))

(defn print-solutions [sols]
  (doseq [sol sols
          :let [i (.indexOf sols sol)]]
    (println (str "Solution #" (+ i 1) ":"))
    (print-solution sol)
    (println)))

(defn mk-callback [cp xs]
  (let [counter (atom 0)
        vars xs
        results (atom [])
        p (proxy [CpSolverSolutionCallback] []
            (onSolutionCallback []
              (swap! counter inc)
              (let [res (map #(proxy-super value %) vars)]
                (swap! results conj res) ;; saves result
                (print-solution res @counter)) ;; prints result to console
              ))]
              ;;(if (>= @counter 10) (proxy-super stopSearch))))]
    {:proxy p
     :results (fn [] @results)
     :count (fn [] @counter)}))

(defn -main [n]
  (load-jni)
  (let [cp-model (mk-cp)
        xs (mk-var-arr cp-model [0 (- n 1)] n)
        cs (add-cs cp-model xs n)
        status (cp-solve cp-model)
        sol-callback (mk-callback cp-model xs)]
    (cp-solve-all cp-model sol-callback)
    (-> (for [r ((:results sol-callback))]
          (-> (solution-printer r)
              (#(clojure.string/join "\n" %))))
        (vec)
        (#(clojure.string/join "\n\n" %))
        (#(spit "resources/nqueens.out" %)))
    (println ((:count sol-callback)))))
