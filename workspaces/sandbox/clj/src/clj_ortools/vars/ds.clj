(ns clj-ortools.vars.ds
  (:gen-class)
  (:import (com.google.protobuf GeneratedMessageV3)
           (com.google.ortools.sat CpModel
                                   CpSolver
                                   CpSolverStatus
                                   IntVar)))

(defn mk-cp []
  {:model (CpModel.)
   :solver (CpSolver.)})

(defn mk-int-var [cp r n]
  (cond
    (= (type r) java.lang.Long)
    (.newIntVar (:model cp) r r n)
    (= (type r) (type {}))
    (let [[l u] (vals r)]
      (.newIntVar (:model cp) l u n))
    :default
    (let [[l u] (vec r)]
      (.newIntVar (:model cp) l u n))))

(defn var-arr [coll]
  (into-array IntVar coll))

(defn cp-solve [cp]
  (.solve (:solver cp) (:model cp)))

(defn result [cp var]
  (.value (:solver cp) var))

(defn cpvar->ds [var]
  (-> var
      bean
      :builder
      bean
      (select-keys [:name :domainList])))

;; (defn cpvar->str [var]
;;   (-> (cpvar->ds var)
;;       str))

(defn vararr->ds [vars]
  (-> (map cpvar->ds vars)
      vec))
