(ns clj-ortools.core
  (:gen-class)
  (:import (com.google.protobuf GeneratedMessageV3)
           (com.google.ortools.sat CpModel
                                   CpSolver
                                   CpSolverStatus
                                   IntVar)))

(defn load-jni []
  (System/loadLibrary "jniortools"))

(defn mk-cp []
  {:model (CpModel.)
   :solver (CpSolver.)})

(defn mk-int-var [cp r n]
  (cond
    (= (type r) java.lang.Long)
    (.newIntVar (:model cp) r r n)
    (= (type r) (type {}))
    (let [[l u] (vals r)]
      (.newIntVar (:model cp) l u n))
    :default
    (let [[l u] (vec r)]
      (.newIntVar (:model cp) l u n))))

(defn var-arr [coll]
  (into-array IntVar coll))

(defn mk-var-arr
  ([cp d n] (mk-var-arr cp d n "x_"))
  ([cp d n prefix] (mk-var-arr cp d 0 n prefix))
  ([cp d l n prefix]
   (var-arr (for [i (range l n)]
              (mk-int-var cp
                          d
                          (format (str prefix "%d") i))))))

(defn cp-solve [cp]
  (.solve (:solver cp) (:model cp)))

(defn cp-solve-all [cp sol-callback]
  (.searchAllSolutions (:solver cp) (:model cp) (:proxy sol-callback)))

(defn result [cp var]
  (.value (:solver cp) var))

(defn got->or [cp l r]
  (let [c1 (.. (:model cp)
               (newBoolVar "or-cond1"))
        c2 (.. (:model cp)
               (newBoolVar "or-cond2"))]
    (.. l (onlyEnforceIf c1))
    (.. r (onlyEnforceIf c2))
    (.. (:model cp) (addBoolOr (var-arr [c1 c2])))
    nil))

(defn got->xor [cp l r]
  (let [c1 (.. (:model cp)
               (newBoolVar "or-cond1"))
        c2 (.. (:model cp)
               (newBoolVar "or-cond2"))]
    (.. l (onlyEnforceIf c1))
    (.. r (onlyEnforceIf c2))
    (.. (:model cp) (addBoolXor (var-arr [c1 c2])))
    nil))

(defn -main []
  (load-jni)
  (let [cp (mk-cp)
        num-vals 3
        x (.newConstant (:model cp) 0);(.newIntVar (:model cp) 0 (- num-vals 1) "x")
        y (.newIntVar (:model cp) 0 (- num-vals 1) "y")
        z (.newConstant (:model cp) 0)];(.newIntVar (:model cp) 0 (- num-vals 1) "z")]
    (.addAllDifferent (:model cp) (into-array IntVar [x y z]))
    (got->or cp
             (.. (:model cp) (addGreaterOrEqualWithOffset z x 1))
             (.. (:model cp) (addLessOrEqualWithOffset y x (- 1))))
    (let [status (cp-solve cp)]
      (cond
          (= status CpSolverStatus/FEASIBLE) (do (println "x = " (.value (:solver cp) x))
                                                 (println "y = " (.value (:solver cp) y))
                                                 (println "z = " (.value (:solver cp) z)))
          :default (println "No Solutions.")))))
