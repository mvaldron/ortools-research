ROOT_WS_DIR=workspaces
IF_RESERVED_NAMES=\( "$(WS_NAME)" = "temp" -o "$(WS_NAME)" = "README.md" -o "$(WS_NAME)" = "Makefile" -o "$(WS_NAME)" = "$(ROOT_WS_DIR)" \)

new:
	if [ ! -z "$(WS_NAME)" -a ! $(IF_RESERVED_NAMES) -a ! -z "$(PLANG)" ]; then mkdir -p $(ROOT_WS_DIR)/$(WS_NAME) && cp -r temp/$(PLANG)/* $(ROOT_WS_DIR)/$(WS_NAME)/; fi

del:
	if [ ! -z "$(WS_NAME)" -a ! $(IF_RESERVED_NAMES) ]; then rm -rf $(ROOT_WS_DIR)/$(WS_NAME); fi
